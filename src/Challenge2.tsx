import React, { useRef, useState } from 'react';
import { createRoot } from 'react-dom/client';
import {
    Card,
    CardBody,
    Row,
    Col,
    Toast,
    ToastHeader,
    ToastBody,
    Button,
    Table
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

interface IFileStagingExceptionProps {
    open: boolean;
    header: string | React.ReactElement;
    body: string | React.ReactElement;
}

const FileStagingExceptionToast: React.FunctionComponent<
    IFileStagingExceptionProps
> = (props: IFileStagingExceptionProps): React.ReactElement => {
    return (
        <Toast isOpen={props.open}>
            <ToastHeader icon='danger'>{props.header}</ToastHeader>
            <ToastBody>{props.body}</ToastBody>
        </Toast>
    );
};

export const Challenge2Component: React.FunctionComponent =
    (): React.ReactElement => {
        const [exceptionToastOpen, setExceptionToastOpen] = useState(false);
        const [exceptionToastBody, setExceptionToastBody] = useState<
            string | React.ReactElement
        >('');
        const [successToastOpen, setSuccessToastOpen] = useState(false);
        const [fileList, setFileList] = useState(new Array<File>());
        const [currentFile, setCurrentFile] = useState<File>(null);
        const inputRef = useRef();

        const fileInputChanged: React.ChangeEventHandler<HTMLInputElement> = (
            e: React.ChangeEvent<HTMLInputElement>
        ) => {
            e.preventDefault();
            const file = e.target.files[0];
            const baseString = `File: ${file.name} cannot be accepted`;
            const fileTooBig = file.size > 10000000;
            const fileSizeString = String((file.size / 1000000).toFixed(2));
            const fileTooBigString = `Size: ${fileSizeString}MB exceeds file size limit: 10MB`;
            const fileWrongType = file.type !== 'application/pdf';
            const fileWrongTypeString = `Type: ${file.name.substring(
                file.name.lastIndexOf('.'),
                file.name.length
            )} is incorrect, only .pdf is accepted`;
            if (fileTooBig || fileWrongType) {
                e.target.value = null;
                setCurrentFile(null);
                //again would happily not use ternaries but they are my favorite
                const toastBody =
                    fileTooBig && fileWrongType ? (
                        <>
                            {baseString}
                            <br />
                            {fileTooBigString}
                            <br />
                            {fileWrongTypeString}
                        </>
                    ) : fileTooBig ? (
                        <>
                            {baseString}
                            <br />
                            {fileTooBigString}
                        </> //fileWrongType
                    ) : (
                        <>
                            {baseString}
                            <br />
                            {fileWrongTypeString}
                        </>
                    );
                setExceptionToastBody(toastBody);
                setExceptionToastOpen(true);
                setTimeout(() => setExceptionToastOpen(false), 7500);
            } else {
                setCurrentFile(file);
            }
        };

        const stageFileButtonClicked: React.MouseEventHandler<
            HTMLButtonElement
        > = () => {
            setFileList(fileList => [...fileList, currentFile]);
            (inputRef.current as HTMLInputElement).value = null;
            setCurrentFile(null);
        };

        const fileRemoveButtonClicked = (idx: number) => {
            const fileListCopy = [...fileList];
            fileListCopy.splice(idx, 1);
            setFileList(fileListCopy);
        };

        const saveButtonClicked = () => {
            //mock server call to send file list
            setFileList([]);
            setSuccessToastOpen(true);
            setTimeout(() => setSuccessToastOpen(false), 5000);
        };

        let hiddenClassName = fileList.length === 0 ? 'd-none' : '';

        return (
            <>
                <FileStagingExceptionToast
                    open={exceptionToastOpen}
                    header='Error'
                    body={exceptionToastBody}
                />
                <Toast isOpen={successToastOpen}>
                    <ToastHeader icon='success'>Success</ToastHeader>
                    <ToastBody>Your files have saved successfully!</ToastBody>
                </Toast>
                <Row className='h-100 justify-content-center align-items-center'>
                    <Col sm={6}>
                        <Card>
                            <CardBody className='table-responsive'>
                                <Row>
                                    <Col sm={10}>
                                        <span>PDF File:</span>
                                        <br />
                                        <input
                                            ref={inputRef}
                                            type='file'
                                            accept='application/pdf'
                                            onChange={fileInputChanged}
                                        />
                                    </Col>
                                    <Col sm={2}>
                                        <Button
                                            className='float-end'
                                            color='secondary'
                                            onClick={stageFileButtonClicked}
                                            disabled={currentFile === null}
                                        >
                                            +
                                        </Button>
                                    </Col>
                                </Row>
                                <hr />
                                <Table className={hiddenClassName}>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Size (MB)</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {fileList.map((f, idx) => (
                                            <tr key={idx}>
                                                <td>{f.name}</td>
                                                <td>
                                                    {(f.size / 1000000).toFixed(
                                                        2
                                                    )}
                                                </td>
                                                <td>
                                                    <Button
                                                        color='danger'
                                                        onClick={() =>
                                                            fileRemoveButtonClicked(
                                                                idx
                                                            )
                                                        }
                                                    >
                                                        -
                                                    </Button>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                                <Button
                                    color='primary'
                                    className={`${hiddenClassName} float-end`}
                                    onClick={() => saveButtonClicked()}
                                >
                                    Save
                                </Button>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    };

export function renderChallenge2(parentId: string): void {
    const root = createRoot(document.getElementById(parentId));
    root.render(<Challenge2Component></Challenge2Component>);
}
