const path = require('path');

module.exports = {
    entry: { 'Challenge2': './src/Challenge2.tsx' },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist/',
        library: {
            name: '[name]',
            type: 'umd'
        },
        clean: true
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    }
};