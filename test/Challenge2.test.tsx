import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Challenge2Component } from '../src/Challenge2';

describe('Challenge2Component', () => {
    it('should have file input', async () => {
        const { container } = render(<Challenge2Component />);
        const input = container.querySelector('input');
        expect(input.type).toBe('file');
    });
    it('should have span label', () => {
        const { container } = render(<Challenge2Component />);
        const span = container.querySelector('span');
        expect(span).toHaveTextContent('PDF File:');
    });
});
